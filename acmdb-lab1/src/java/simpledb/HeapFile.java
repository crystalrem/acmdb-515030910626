package simpledb;

import java.io.*;
import java.util.*;

/**
 * HeapFile is an implementation of a DbFile that stores a collection of tuples
 * in no particular order. Tuples are stored on pages, each of which is a fixed
 * size, and the file is simply a collection of those pages. HeapFile works
 * closely with HeapPage. The format of HeapPages is described in the HeapPage
 * constructor.
 * 
 * @see simpledb.HeapPage#HeapPage
 * @author Sam Madden
 */
public class HeapFile implements DbFile {

    /**
     * Constructs a heap file backed by the specified file.
     * 
     * @param f
     *            the file that stores the on-disk backing store for this heap
     *            file.
     */
	private File file;
	private TupleDesc tupleDesc;
    public HeapFile(File f, TupleDesc td) {
        // some code goes here
    	file = f;
    	tupleDesc = td;
    }

    /**
     * Returns the File backing this HeapFile on disk.
     * 
     * @return the File backing this HeapFile on disk.
     */
    public File getFile() {
        // some code goes here
        //return null;
    	return file;
    }

    /**
     * Returns an ID uniquely identifying this HeapFile. Implementation note:
     * you will need to generate this tableid somewhere ensure that each
     * HeapFile has a "unique id," and that you always return the same value for
     * a particular HeapFile. We suggest hashing the absolute file name of the
     * file underlying the heapfile, i.e. f.getAbsoluteFile().hashCode().
     * 
     * @return an ID uniquely identifying this HeapFile.
     */
    public int getId() {
        // some code goes here
        //throw new UnsupportedOperationException("implement this");
    	return file.getAbsoluteFile().hashCode();
    }

    /**
     * Returns the TupleDesc of the table stored in this DbFile.
     * 
     * @return TupleDesc of this DbFile.
     */
    public TupleDesc getTupleDesc() {
        // some code goes here
        //throw new UnsupportedOperationException("implement this");
    	return tupleDesc;
    }

    // see DbFile.java for javadocs
    public Page readPage(PageId pid) {
        // some code goes here
        //return null;
    	int offset = pid.pageNumber() * BufferPool.getPageSize();
        byte[] buf = new byte[BufferPool.getPageSize()];
        Page p;
        try{
            RandomAccessFile raf = new RandomAccessFile(file, "r");
            raf.seek(offset);
            raf.read(buf, 0, BufferPool.getPageSize());
            p = new HeapPage((HeapPageId)pid, buf);
            raf.close();
            return p;
        } catch (Exception e){
            System.out.println("RandomAccessFile couldnt open");
        }
        return null;    	
    }

    // see DbFile.java for javadocs
    public void writePage(Page page) throws IOException {
        // some code goes here
        // not necessary for lab1
    }

    /**
     * Returns the number of pages in this HeapFile.
     */
    public int numPages() {
        // some code goes here
        //return 0;
    	return (int)Math.ceil(file.length() / BufferPool.getPageSize());
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> insertTuple(TransactionId tid, Tuple t)
            throws DbException, IOException, TransactionAbortedException {
        // some code goes here
        return null;
        // not necessary for lab1
    }

    // see DbFile.java for javadocs
    public ArrayList<Page> deleteTuple(TransactionId tid, Tuple t) throws DbException,
            TransactionAbortedException {
        // some code goes here
        return null;
        // not necessary for lab1
    }

    // see DbFile.java for javadocs
    class HeapFileIterator extends AbstractDbFileIterator{
    
    	private Iterator<Tuple> tupleIt = null;
        private TransactionId tId = null;
        private int pageNum = 0;
        private HeapFile file = null;
        public HeapFileIterator(TransactionId tid , HeapFile f){
            this.tId = tid;
            this.file = f;
        }
        
    	public void open() throws DbException, TransactionAbortedException{
    		pageNum = -1;
    	}
    	
    	public boolean hasNext() throws DbException, TransactionAbortedException{
    		Iterator<Tuple> tmp = tupleIt;
    		int pgNo = pageNum;
    		if (tmp != null && !tmp.hasNext()) tmp = null;
			while (tmp == null && pgNo < file.numPages() - 1) {
				pgNo++;
				HeapPageId pageId = new HeapPageId(file.getId(), pgNo);
				HeapPage page = (HeapPage) Database.getBufferPool().getPage(tId, pageId, Permissions.READ_ONLY);
				tmp = page.iterator();

				// Make sure the iterator has tuples in it
				if (!tmp.hasNext()) tmp = null;
			}

			// Make sure we found a tuple iterator
			if (tmp == null)
				return false;

			// Return the next tuple.
			return true;
    	}

    	public Tuple next() throws DbException, TransactionAbortedException, NoSuchElementException{
    		if (tupleIt != null && !tupleIt.hasNext()) tupleIt = null;
			while (tupleIt == null && pageNum < file.numPages() - 1) {
				pageNum++;
				HeapPageId pageId = new HeapPageId(file.getId(), pageNum);
				HeapPage page = (HeapPage) Database.getBufferPool().getPage(tId, pageId, Permissions.READ_ONLY);
				tupleIt = page.iterator();
				// Make sure the iterator has tuples in it
				if (tupleIt != null && !tupleIt.hasNext())
					tupleIt = null;
			}

			if (tupleIt == null) throw new NoSuchElementException("HeapFile: Class HeapFileIterator: next()");
			return tupleIt.next();
    	}

    	public void rewind() throws DbException, TransactionAbortedException{
    		close();
			open();
    	
    	}

    	public void close(){
    		super.close();
    		pageNum = Integer.MAX_VALUE;
    		tupleIt = null;
    		
    	}
		@Override
		protected Tuple readNext() throws DbException,
			TransactionAbortedException {
			// TODO Auto-generated method stub
			return null;
		}
    }
    public DbFileIterator iterator(TransactionId tid) {
        // some code goes here
    	return new HeapFileIterator(tid, this);
        //return null;
    }
    

}

