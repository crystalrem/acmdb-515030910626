package simpledb;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.TreeMap;

/**
 * Knows how to compute some aggregate over a set of IntFields.
 */
public class IntegerAggregator implements Aggregator {

    private static final long serialVersionUID = 1L;

    /**
     * Aggregate constructor
     * 
     * @param gbfield
     *            the 0-based index of the group-by field in the tuple, or
     *            NO_GROUPING if there is no grouping
     * @param gbfieldtype
     *            the type of the group by field (e.g., Type.INT_TYPE), or null
     *            if there is no grouping
     * @param afield
     *            the 0-based index of the aggregate field in the tuple
     * @param what
     *            the aggregation operator
     */
    private Type gbfieldType;
    private int gbfield, afield;
    private Op what;
    private Object aggr;
    public IntegerAggregator(int gbfield, Type gbfieldtype, int afield, Op what) {
        // some code goes here
    	this.gbfieldType = gbfieldtype;
    	this.afield = afield;
    	this.gbfield = gbfield;
    	this.what = what;
    	if (this.gbfield == Aggregator.NO_GROUPING) {
            aggr = (Object) new ArrayList<Integer>();
        } else {
            if (gbfieldtype == Type.INT_TYPE) aggr = (Object) new TreeMap<Integer, ArrayList<Integer>>();
            else aggr = (Object) new TreeMap<String, ArrayList<Integer>>();
        }
    }

    /**
     * Merge a new tuple into the aggregate, grouping as indicated in the
     * constructor
     * 
     * @param tup
     *            the Tuple containing an aggregate field and a group-by field
     */
    public void mergeTupleIntoGroup(Tuple tup) {
        // some code goes here
    	if (this.gbfield == Aggregator.NO_GROUPING) {
            ((ArrayList<Integer>) aggr).add(((IntField)tup.getField(afield)).getValue());
        } else {
            if (gbfieldType == Type.INT_TYPE) {
                TreeMap<Integer, ArrayList<Integer>> groupAggr = (TreeMap<Integer, ArrayList<Integer>>) aggr;
                Integer key = ((IntField) tup.getField(gbfield)).getValue();
                Integer val = ((IntField) tup.getField(afield)).getValue();
                if (!groupAggr.containsKey(key)) groupAggr.put(key, new ArrayList<Integer>(1));
                groupAggr.get(key).add(val);
            } else if (gbfieldType == Type.STRING_TYPE) {
                TreeMap<String, ArrayList<Integer>> groupAggr = (TreeMap<String, ArrayList<Integer>>) aggr;
                String key = ((StringField) tup.getField(gbfield)).getValue();
                Integer val = ((IntField) tup.getField(afield)).getValue();
                if (!groupAggr.containsKey(key)) groupAggr.put(key, new ArrayList<Integer>(1));
                groupAggr.get(key).add(val);
            }
        }
    	
    }

    /**
     * Create a DbIterator over group aggregate results.
     * 
     * @return a DbIterator whose tuples are the pair (groupVal, aggregateVal)
     *         if using group, or a single (aggregateVal) if no grouping. The
     *         aggregateVal is determined by the type of aggregate specified in
     *         the constructor.
     */
    public DbIterator iterator() {
        // some code goes here
        return new AggregatorDbIterator();
    }

    private class AggregatorDbIterator implements DbIterator {
        private ArrayList<Tuple> results;
        private Iterator<Tuple> iter;

        public int calcAggrRes(ArrayList<Integer> list) {
            int result = 0;
            if(what == Op.MIN) {
            	result = list.get(0);
                for (int v : list) {
                    if (result > v) result = v;
                }
            }
            else if(what == Op.MAX) {
            	result = list.get(0);
                for (int v : list) {
                    if (result < v) result = v;
                }
            }
            else if(what == Op.SUM) { 
            	for (int v : list) result += v;
            }
            else if(what == Op.AVG) {
            	for (int v : list) result += v;
                result = result / list.size();
            }
            else if(what == Op.COUNT) {
            	result = list.size();
            }
            else {
            	try {
                    throw new DbException("IntegerAggregator: Iterator - calcAggr not implemented");
                } catch (DbException e) {
                    e.printStackTrace();
                }
            }
            return result;
        }

        public AggregatorDbIterator() {
        	results = new ArrayList<Tuple>();
            if (gbfield == Aggregator.NO_GROUPING) {
                Tuple t = new Tuple(getTupleDesc());
                Field aggregateVal = new IntField(this.calcAggrRes((ArrayList<Integer>) aggr));
                t.setField(0, aggregateVal);
                results.add(t);
            } else {
                for (Map.Entry e : ((TreeMap<Integer, ArrayList<Integer>>) aggr).entrySet()) {
                    Tuple tuple = new Tuple(getTupleDesc());
                    Field value = null;
                    if (gbfieldType == Type.INT_TYPE) value = new IntField((int) e.getKey());
                    else {
                        String str = (String) e.getKey();
                        value = new StringField(str, str.length());
                    }
                    Field aggregateVal = new IntField(this.calcAggrRes((ArrayList<Integer>) e.getValue()));
                    tuple.setField(0, value);
                    tuple.setField(1, aggregateVal);
                    results.add(tuple);
                }
            }
        }
        @Override
        public void open() throws DbException, TransactionAbortedException {
        	iter = results.iterator();
        }
        @Override
        public boolean hasNext() throws DbException, TransactionAbortedException {
            if (iter == null) {
                throw new IllegalStateException("IntegerAggregator: Iterator - !hasNext");

            }
            return iter.hasNext();

        }
        @Override
        public Tuple next() throws DbException, TransactionAbortedException, NoSuchElementException {
            if (iter == null)  throw new IllegalStateException("IntegerAggregator: Iterator - !next");
            return iter.next();
        }
        @Override
        public void rewind() throws DbException, TransactionAbortedException {
            if (iter == null) throw new IllegalStateException("IntegerAggregator: Iterator - rewind");
            iter = results.iterator();
        }
        @Override
        public TupleDesc getTupleDesc() {
            if (gbfield == Aggregator.NO_GROUPING) return new TupleDesc(new Type[]{Type.INT_TYPE}); 
            else return new TupleDesc(new Type[]{gbfieldType, Type.INT_TYPE});
        }
        @Override
        public void close() {
        	iter = null;
        }
    }

}